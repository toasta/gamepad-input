This project visualizes an approach of text input via gamepad that i remember from a homebrew program for the PSP (i cannot remember which one. if you know, please tell me!)

# Todo
- [x] visual layout of the input grid
- [x] controls via gamepad API
- [x] basic text field
- keyboard layout
    - [ ] implement numbers
    - [ ] implement special characters
- implement editor functionality
    - [ ] deleting characters
    - [ ] moving the cursor
    - [ ] making a selection
    - [ ] copying / pasting
