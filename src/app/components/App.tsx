import * as React from 'react';
import { Gamepad } from './Gamepad';
import { useForceUpdate } from '../helper/useForceUpdate';
import { Input } from './Input';

interface IAppState {
    gamepad: Gamepad | null;
}

export class App extends React.Component<{}, IAppState> {
    state = {
        gamepad: null
    };

    tick = () => {
        this.setState({
            gamepad: this.pollGamepad()
        });
        window.requestAnimationFrame(() => this.tick())
    }

    pollGamepad = () => {
        return navigator.getGamepads()[0];
    }

    componentDidMount() {
        this.tick();
    }

    render() {
        if (!this.state.gamepad)
            return 'please connect a gamepad';

        return (
            <Gamepad gamepad={this.state.gamepad} />
        )
    }
}