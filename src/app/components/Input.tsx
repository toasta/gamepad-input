import styled from "@emotion/styled";
import { theme } from "../config/theme";

export const Input = styled.input({
    backgroundColor: 'transparent',
    padding: `${theme.unit}px ${theme.unit * 2}px`,
    borderRadius: theme.unit,
    borderColor: theme.activeCornerBackground,
    borderStyle: 'solid',
    borderWidth: 2,
    color: theme.color,
    outline: 'none',
    fontSize: theme.unit * 2,

    '&:focus': {
        borderColor: theme.activeCharBackground
    }
});