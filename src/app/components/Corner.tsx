import styled from "@emotion/styled";
import { TCorner } from "../config/mapping";
import { theme } from "../config/theme";

export const AREA_TOP = 'top';
export const AREA_RIGHT = 'right';
export const AREA_BOTTOM = 'bottom';
export const AREA_LEFT = 'left';

export const Corner = styled.div({
    // display: 'grid',
    // gridTemplateColumns: '1fr 1fr 1fr',
    // gridTemplateRows: '1fr 1fr 1fr',
    // gridTemplateAreas: `
    //     '. ${AREA_TOP} .'
    //     '${AREA_LEFT} . ${AREA_RIGHT}'
    //     '. ${AREA_BOTTOM} .'
    // `,

    position: 'relative',

    borderRadius: '50%',
    
    '&.active': {
        backgroundColor: theme.activeCornerBackground,
        color: theme.activeColor
    }
});