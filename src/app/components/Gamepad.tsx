import * as React from 'react';
import { GamepadInput } from './GamepadInput';

interface IGamepadProps {
    gamepad: Gamepad;
}

interface IGamepadState {
    charIndex: number | null;
    cornerIndex: number | null;
    layerIndex: number;
}

const initialState: IGamepadState = {
    charIndex: null,
    cornerIndex: 4,
    layerIndex: 0
}

const getGamepadInputState = (gamepad: Gamepad): IGamepadState => {
    let horizontalIndex = Math.floor((gamepad.axes[0] + 1) * 1.5)
    let verticalIndex = Math.floor((gamepad.axes[1] + 1) * 1.5)

    if (horizontalIndex === 3)
        horizontalIndex--;

    if (verticalIndex === 3)
        verticalIndex--;

    const cornerIndex = horizontalIndex + verticalIndex * 3;



    const layerIndex = gamepad.buttons[4].pressed
        ? 1
        : 0;

    
    let charIndex = null;
    if (gamepad.buttons[2].pressed)
        charIndex = 0;
    if (gamepad.buttons[0].pressed)
        charIndex = 1;
    if (gamepad.buttons[1].pressed)
        charIndex = 2;
    if (gamepad.buttons[3].pressed)
        charIndex = 3;
        
    return {
        charIndex,
        cornerIndex,
        layerIndex
    }
}

export const Gamepad = ({ gamepad }: IGamepadProps) => {
    const [state, setState] = React.useState<IGamepadState>(initialState);

    React.useEffect(() => {
        setState(getGamepadInputState(gamepad));
    }, [
        gamepad.axes[0], 
        gamepad.axes[1], 
        gamepad.buttons[4],
        gamepad.buttons[2],
        gamepad.buttons[0],
        gamepad.buttons[1],
        gamepad.buttons[3],
    ]);


    return (
        <GamepadInput {...state} />
    )
}

