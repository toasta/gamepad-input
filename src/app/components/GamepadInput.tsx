import * as React from 'react';
import { mapping } from '../config/mapping';
import { Corner } from './Corner';
import { Char } from './Char';
import { Mapping } from './Mapping';
import { Input } from './Input';

interface IGamepadInputProps {
    charIndex: number | null;
    cornerIndex: number | null;
    layerIndex: number;
}

export const GamepadInput = (props: IGamepadInputProps) => {
    const { charIndex, cornerIndex, layerIndex } = props;

    const [text, setText] = React.useState('');

    React.useEffect(() => {
        if (charIndex !== null) {
            try {
                const char = mapping[cornerIndex][charIndex][layerIndex];
                setText(text + char);
            } catch (e) {
                // no mapping available
            }
        }
    }, [charIndex])

    return (
        <>
            <Input
                value={text}
                onChange={e => setText(e.target.value)}
            />
            <Mapping>
                {mapping.map((corner, index) => {
                    const isActiveCorner = index === cornerIndex;
                    const className = isActiveCorner ? 'active' : '';

                    return (
                        <Corner
                            key={`corner-${index}`}
                            className={className}
                        >
                            {corner.map((char, index) => {
                                const isActiveChar = index === charIndex;
                                const className = isActiveCorner && isActiveChar ? 'active' : '';
                                return (
                                    <Char
                                        key={`char-${index}`}
                                        className={className}
                                    >
                                        {char[layerIndex]}
                                    </Char>
                                )
                            })}
                        </Corner>
                    )
                })}
            </Mapping>
        </>
    )
}