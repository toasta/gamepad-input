import styled from "@emotion/styled";
import { theme } from "../config/theme";

const size = theme.unit * 30;

export const Mapping = styled.div({
    display: 'grid',
    padding: theme.unit,
    gridTemplateColumns: '1fr 1fr 1fr',
    gridTemplateRows: '1fr 1fr 1fr',

    width: size,
    height: size,

    fontFamily: '"Fira Mono"',
    color: theme.color
});