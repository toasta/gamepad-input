import styled from '@emotion/styled';
import { theme } from '../config/theme';

const radius = (1 / (1 + Math.sqrt(2))) * 100;

const left: React.CSSProperties = {
    left: 0,
    top: 50 - radius / 2 + '%'
}

const bottom: React.CSSProperties = {
    bottom: 0,
    left: 50 - radius / 2 + '%'
}

const right: React.CSSProperties = {
    right: 0,
    top: 50 - radius / 2 + '%'
}

const top: React.CSSProperties = {
    top: 0,
    left: 50 - radius / 2 + '%'
}

export const Char = styled.div({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 500,
    fontSize: theme.unit * 2,
    
    borderRadius: '50%',
    width: radius + '%',
    height: radius + '%',
    position: 'absolute',

    '&:nth-child(1)': left,

    '&:nth-child(2)': bottom,

    '&:nth-child(3)': right,

    '&:nth-child(4)': top,

    '&.active': {
        backgroundColor: theme.activeCharBackground
    }
});