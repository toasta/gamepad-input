import * as React from 'react';

export const useForceUpdate = () => {
    const [, setIt] = React.useState(false);
    return () => setIt(it => !it);
}