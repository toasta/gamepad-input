// a single character
export type TChar = (string | null)[];

// a group of four characters
export type TCorner = TChar[];

// a group of nine corners
export type TMapping = TCorner[];

type TMetaChar = string[];
type TMetaCorner = TMetaChar[];
type TMetaMapping = TMetaCorner[];

export const mapping: TMapping = [
    [
        [',', ','],
        [' ', ' '],
        ['.', '.'],
        ['!', '!']
    ],
    [
        ['a', 'A'],
        ['b', 'B'],
        ['c', 'C']
    ],
    [
        ['d', 'D'],
        ['e', 'E'],
        ['f', 'F']
    ],
    [
        ['g', 'G'],
        ['h', 'H'],
        ['i', 'I']
    ],
    [
        ['j', 'J'],
        ['k', 'K'],
        ['l', 'L']
    ],
    [
        ['m', 'M'],
        ['n', 'N'],
        ['o', 'O']
    ],
    [
        ['p', 'P'],
        ['q', 'Q'],
        ['r', 'R'],
        ['s', 'S']
    ],
    [
        ['t', 'T'],
        ['u', 'U'],
        ['v', 'V']
    ],
    [
        ['w', 'W'],
        ['x', 'X'],
        ['y', 'Y'],
        ['z', 'Z']
    ]
]