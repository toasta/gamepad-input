import tinycolor from 'tinycolor2';

export const theme = {
    unit: 16,
    color: '#000000CC',
    activeColor: '#FFFFFFCC',
    activeCornerBackground: '#555',
    activeCharBackground: '#222',
}