import * as React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components/App';
import 'typeface-fira-mono';

ReactDOM.render(<App/>, document.getElementById('app'))